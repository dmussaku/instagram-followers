from django.conf.urls import patterns, include, url
from django.contrib import admin
from instapp.views import log_view, follower_statistics, stats, index
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from instagram_followers import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'instagram_followers.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', index, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logs/$', log_view),
    url(r'^stats/$', stats, name='stats'),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)