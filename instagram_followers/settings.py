"""
Django settings for instagram_followers project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

def rel(*x):
    return os.path.join(BASE_DIR, *x)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-^ci6blpamz$g496&fw=yj#&)f7)8p9ssery=bfo2&y_ppsf2)'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'instapp',
    'django_extensions', 'djcelery',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

import djcelery
djcelery.setup_loader()

ROOT_URLCONF = 'instagram_followers.urls'

WSGI_APPLICATION = 'instagram_followers.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'djangod.b.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, '../instagram.db3'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'instagram',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Almaty'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = BASE_DIR + '/static/'
# TEMPLATES_FOLDER = BASE_DIR + '/static/templates/'
# STATIC_ROOT = '/'
STATIC_URL = '/static/'
STATIC_ROOT = rel('..', 'static')
STATICFILES_DIRS = (
    rel('static'),
)

INSTAVIDEO_KAZ_ID='1280796486'

INSTAGRAM_USER_DAN = {
    'CLIENT_ID':'7e028dde82ac4c9aa244effc013a0274',
    'CLIENT_SECRET':'e1c97ca959554ac59a149efbf8f4fc22',
    'ACCESS_TOKEN':'46695212.7e028dd.e4ce491f1aff44cf8b4315af0293d9f8',
    'USER_ID':'46695212'
}

INSTAGRAM_USER_DAN_RESPONSE = (u'46695212.7e028dd.e4ce491f1aff44cf8b4315af0293d9f8', 
                                {u'username': u'dmussaku', 
                                 u'bio': u'\u041d\u0435 \u043b\u044e\u0431\u043b\u044e \u043f\u043e\u0432\u0442\u043e\u0440\u044f\u0442\u044c\u0441\u044f... \u043d\u0435 \u043b\u044e\u0431\u043b\u044e \u043f\u043e\u0432\u0442\u043e\u0440\u044f\u0442\u044c\u0441\u044f, \u043d\u0435 \u043b\u044e\u0431\u043b\u044e \u043f\u043e\u0432\u0442\u043e\u0440\u044f\u0442\u044c\u0441\u044f', 
                                 u'website': u'', 
                                 u'profile_picture': u'https://instagramimages-a.akamaihd.net/profiles/profile_46695212_75sq_1335285952.jpg', 
                                 u'full_name': u'Gromilla Solomon', 
                                 u'id': u'46695212'}
                                )

INSTAGRAM_USER_DUMAN_RC = {
    'CLIENT_ID':'d3e7c492acc844f08f7d457110f47dea',
    'CLIENT_SECRET':'44e9f649425a46b883d8f5363a23c476',
    'ACCESS_TOKEN':'1535769343.d3e7c49.5f4b46f94943430baaac4cbe117563cd',
    'USER_ID':'1535769343'
}

INSTAGRAM_USER_DUMAN_RC_RESPONSE = (u'1535769343.d3e7c49.5f4b46f94943430baaac4cbe117563cd', 
    {u'username': u'duman_rc', 
    u'bio': u'\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0441 \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0439, \u0441\u043e\u0441\u0442\u043e\u044f\u0449\u0438\u0439 \u0438\u0437: 5D-\u043a\u0438\u043d\u043e\u0442\u0435\u0430\u0442\u0440\u0430, \xab\u0414\u0436\u0443\u043d\u0433\u043b\u0438\xbb, \u0430\u0432\u0438\u0430-\u0441\u0438\u043c\u0443\u043b\u044f\u0442\u043e\u0440\u0430 \u0438 \u043e\u043a\u0435\u0430\u043d\u0430\u0440\u0438\u0443\u043c\u0430 \u0441 \u0443\u0441\u043b\u0443\u0433\u0430\u043c\u0438 \u044d\u043a\u0441\u043f\u0440\u0435\u0441\u0441 \u043f\u043e\u0433\u0440\u0443\u0436\u0435\u043d\u0438\u044f \u0438 \u0430\u043a\u0432\u0430\u0440\u0438\u0443\u043c\u0438\u0441\u0442\u0438\u043a\u0438', 
    u'website': u'http://www.duman.kz', 
    u'profile_picture': u'https://igcdn-photos-e-a.akamaihd.net/hphotos-ak-xfa1/10735222_1484017888548100_1716047697_a.jpg', 
    u'full_name': u'\u0420\u0430\u0437\u0432\u043b\u0435\u043a\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0446\u0435\u043d\u0442\u0440 \u0414\u0443\u043c\u0430\u043d', 
    u'id': u'1535769343'})

INSTAGRAM_USER_ZI_COLLECTION = {
    'CLIENT_ID':'ef337fdf9223483b9f71f242222e7a0b',
    'CLIENT SECRET':'fd81364a2a444c86964ac5078a04970f',
    'ACCESS_TOKEN':'1690175768.ef337fd.dcebefd2816a4f30a09b4691444c0c03',
    'USER_ID':'1690175768'
}

INSTAGRAM_USER_ZI_COLLECTION_RESPONSE = (u'1690175768.ef337fd.dcebefd2816a4f30a09b4691444c0c03', 
    {u'username': u'zi_collection', 
    u'bio': u'Fashion room! \u0421\u0442\u0438\u043b\u044c\u043d\u044b\u0435 \u043d\u0430\u0440\u044f\u0434\u044b, \u044d\u043a\u0441\u043a\u043b\u044e\u0437\u0438\u0432\u043d\u044b\u0435 \u043f\u043b\u0430\u0442\u044c\u044f, \u043c\u043e\u0434\u043d\u044b\u0435 \u043c\u0435\u0445\u043e\u0432\u044b\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f! \u0423\u0441\u043b\u0443\u0433\u0438 \u043f\u043e \u043f\u043e\u0434\u0431\u043e\u0440\u0443 \u0441\u0442\u0438\u043b\u044f. 87019517776 wats up. \u0416\u0434\u0435\u043c \u0412\u0430\u0441!', u'website': u'', u'profile_picture': u'https://igcdn-photos-g-a.akamaihd.net/hphotos-ak-xfa1/t51.2885-19/10946489_1582896415278558_1325781862_a.jpg', u'full_name': u'zi_collection', u'id': u'1690175768'})

INSTAGRAM_USER_TETRUS_CLUB = {
    'CLIENT_ID':'0031ba7387d9463ea86c472aeb8649f7',
    'CLIENT SECRET':'67d04f90da494c768d97c50a0c05ed23',
    'ACCESS_TOKEN':'1709044838.0031ba7.46f9801d0e4543059de3d735a7041187',
    'USER_ID':'1709044838'
}

INSTAGRAM_USER_TETRUS_CLUB = (u'1709044838.0031ba7.46f9801d0e4543059de3d735a7041187', 
    {u'username': u'tetrus.club', 
    u'bio': u'\u041a\u043b\u0443\u0431 \u0434\u0430\u043b\u044c\u043d\u043e\u0432\u0438\u0434\u043d\u044b\u0445 \u043b\u044e\u0434\u0435\u0439.                \u041d\u0430\u0448\u0430 \u0446\u0435\u043b\u044c - \u0440\u0430\u0437\u0432\u0438\u0432\u0430\u0442\u044c \u0438\u043d\u043d\u043e\u0432\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u0434\u0443\u0445 \u0441\u0440\u0435\u0434\u0438 \u043c\u043e\u043b\u043e\u0434\u0435\u0436\u0438, \u0432\u044b\u044f\u0432\u043b\u044f\u0442\u044c, \u0438 \u043f\u043e\u0434\u0434\u0435\u0440\u0436\u0438\u0432\u0430\u0442\u044c \u0442\u0430\u043b\u0430\u043d\u0442\u043b\u0438\u0432\u044b\u0445 \u043b\u044e\u0434\u0435\u0439.', u'website': u'http://www.tetrus.club', u'profile_picture': u'https://igcdn-photos-c-a.akamaihd.net/hphotos-ak-xaf1/t51.2885-19/10963989_860591617332466_2080357873_a.jpg', u'full_name': u'', u'id': u'1709044838'})