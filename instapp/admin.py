from django.contrib import admin
from .models import Client, InstagramUser, FollowerStat, Follower

admin.site.register(Client)
admin.site.register(InstagramUser)
admin.site.register(FollowerStat)
admin.site.register(Follower)
# Register your models here.
