from .models import Client, InstagramUser, FollowerStat, Follower
from instagram.client import InstagramAPI
import time
from random import randint, random
from celery.task import periodic_task, task
from celery.task.schedules import crontab

'''
Needs to be a periodic task before the following procedure
starts, so that the base of client's followers are always up
to date
'''
def populate_follower_db(username):
    c = Client.objects.get(username=username)
    followers = InstagramUser.objects.filter(client=c)
    if (followers):
        instagram_followers = c.get_followers()
        instagram_followers.reverse()
        new_followers = filter(lambda x: x in followers, instagram_followers)
        for follower in new_followers:
            InstagramUser.save_from_instagram_user(follower, c)
    else:
        followers = c.get_followers()
        followers.reverse()
        for follower in followers:
            InstagramUser.save_from_instagram_user(follower, c)

'''
Periodic task to calculate and save the number of followers per day
'''
def followers_per_day(username):
    c = Client.objects.get(username=username)
    f = FollowerStat(client=c, followers_num=len(InstagramUser.objects.filter(client=c)))
    f.save()

'''
This is a follow routine with this logic
1) Get Client and get his current follower
2) See if follower has his followers if not then get them
3) See if there is next element index in InstagramUser if there is
follow 60 users and save the next_elem user_id to continue later. 
If it was the last elem mark InstagramUser as finished and change 
client's current_follower to a new follower 
'''
def follow_routine(client_username):
    time.sleep(randint(50,100))
    COUNT = 18
    client = Client.objects.get(username=client_username)
    api = InstagramAPI(access_token = client.access_token)
    if not InstagramUser.objects.filter(client=client):
        populate_follower_db(client.username)
    current_follower = client.get_current_follower()
    is_requestable = False
    while not is_requestable:
        try:
            users = current_follower.get_follower_list()
            is_requestable = True
        except:
            client.current_follower = client.instagramuser_set.filter(finished=False).first()
            client.save()
    if not current_follower.next_elem:
        current_follower.next_elem = users[0]
    CURRENT_POSITION = users.index(current_follower.next_elem)
    if (len(users)<=COUNT+CURRENT_POSITION):
        COUNT = len(users)-CURRENT_POSITION
    while(COUNT):
        print users[CURRENT_POSITION]
        relationship = api.user_relationship(users[CURRENT_POSITION])
        print relationship.__dict__
        time.sleep(randint(2,3))
        '''
        # outgoing_status - i am following
        # incoming_status - he is following
        '''
        if (relationship.outgoing_status=='none' and relationship.incoming_status=='none'):
            api.follow_user(user_id=users[CURRENT_POSITION])
            print 'followed user %s' % (users[CURRENT_POSITION])
            time.sleep(randint(2,3))
            if (relationship.target_user_is_private == False):
                feed = api.user_media_feed(user_id=users[CURRENT_POSITION])
                media = feed[0][0]
                time.sleep(randint(2,3))
                api.like_media(media_id=media.id)
                print "liked media %s" % (media.id)
                time.sleep(randint(2,3))
            COUNT -= 1
            print "Count is %s" % (COUNT)
        '''
        # if we reached the end of the user list, we should exit and change current
        # follower to the next one and mark that user - finished
        '''
        CURRENT_POSITION += 1
        print "current position is %s out of %s" % (CURRENT_POSITION, len(users))
        try:
            current_follower.next_elem = users[CURRENT_POSITION]
        except:
            pass
        current_follower.save()
        print 'next element is %s' % (current_follower.next_elem)
        if (CURRENT_POSITION==len(users)):
            current_follower.finished = True
            current_follower.save()
            client.current_follower = InstagramUser.objects.get(id=current_follower.id+1)
            client.save()
            break

def instagram_follow(client):
    api = InstagramAPI(access_token=client.access_token)
    COUNT=randint(18,20)
    api_exceptions=0
    if client.follows_num >7000:
        return False
    while COUNT:
        if api_exceptions>=20:
            return False
        
        if client.current_user_id==Follower.objects.last().id:
            return False
        if not client.current_user_id:
            current_follower = Follower.objects.first()
            client.current_user_id = current_follower.id
            client.save()
        else:
            id_exists = False
            i = 1
            while not id_exists:
                try:
                    current_follower = Follower.objects.get(
                        id=client.current_user_id+i
                        )
                    id_exists = True
                except:
                    i = i+1
        try:
            relationship = api.user_relationship(current_follower.user_id)
        except:
            api_exceptions += 1
            client.current_user_id = Follower.objects.get(id=current_follower.id+1).id
            client.save()
            time.sleep(1+random()+random())
            continue
        print str(relationship.__dict__) + ' for user %s with id %s' % (current_follower.username, current_follower.user_id)
        time.sleep(1+random()+random())
        if (relationship.outgoing_status=='none' and relationship.incoming_status=='none'):
            api.follow_user(user_id=current_follower.user_id)
            client.current_user_id = current_follower.id
            client.save()
            print 'followed user %s' % (current_follower.username)
            time.sleep(1+random()+random())
            if (relationship.target_user_is_private == False):
                feed = api.user_media_feed(user_id=current_follower.user_id)
                media = feed[0][0]
                time.sleep(1+random()+random())
                api.like_media(media_id=media.id)
                print "liked media %s" % (media.id)
                time.sleep(1+random()+random())
            COUNT -= 1
        else:
            client.current_user_id = Follower.objects.get(id=current_follower.id+1).id
            client.save()
        print COUNT

def instagram_like(client):
    api = InstagramAPI(access_token=client.access_token)
    COUNT=randint(27,30)
    while COUNT:
        if client.current_user_id==Follower.objects.last().id:
            return False
        if not client.current_user_id:
            current_follower = Follower.objects.first()
            client.current_user_id = current_follower.id
            client.save()
        else:
            id_exists = False
            i = 1
            while not id_exists:
                try:
                    current_follower = Follower.objects.get(
                        id=client.current_user_id+i
                        )
                    id_exists = True
                except:
                    i = i+1
        try:
            relationship = api.user_relationship(current_follower.user_id)
        except:
            client.current_user_id = Follower.objects.get(id=current_follower.id+1).id
            client.save()
            continue
        if (relationship.outgoing_status=='none' and relationship.incoming_status=='none'):
            client.current_user_id = current_follower.id
            client.save()
            time.sleep(1+random()+random())
            if (relationship.target_user_is_private == False):
                feed = api.user_media_feed(user_id=current_follower.user_id)
                media = feed[0][0]
                time.sleep(1+random()+random())
                api.like_media(media_id=media.id)
                print "liked media %s" % (media.id)
                COUNT -= 1
            print "Count is %s" % (COUNT)
        else:
            client.current_user_id = Follower.objects.get(id=current_follower.id+1).id
            client.save()

def instagram_unfollow(client):
    api = InstagramAPI(access_token=client.access_token)
    time.sleep(1+random()+random())
    follows = api.user_follows()[0]
    for i in range(0, 30):
        api.unfollow_user(user_id=follows[i].id)
        time.sleep(1+random()+random())

def insta_task(username):
    client = Client.objects.get(username=username)
    if client.following and client.active:
        print "STARTING FOLLOW ROUTINE FOR %s" % username
        instagram_follow(client)
        print "FINISHED FOLLOW ROUTINE FOR %s" % username
    elif not client.following and client.active:
        print "STARTING UNFOLLOW ROUTINE FOR %s" % username
        instagram_unfollow(client)
        print "FINISHED UNFOLLOW ROUTINE FOR %s" % username
    else:
        return False

def stat_record(username):
    client = Client.objects.get(username=username)
    if client.active:
        obj = FollowerStat(
            client=client,
            followers_num=client.get_followers_num()
            )
        obj.save()
        client.follower_num = obj.followers_num
        # client.follows_num = obj.follows_num
        client.save()


@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def duman_follow():
    insta_task('duman_rc')

# @periodic_task(run_every=crontab(hour='*/1', minute='10'))
# def tetrus_follow():
#     client = Client.objects.get(username='tetrus.club')
#     print "STARTING FOLLOW ROUTINE FOR Tetrus.Club"
#     instagram_follow(client)
#     print "FINISHED FOLLOW ROUTINE FOR Tetrus.Club"

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def kuralays_follow():
    insta_task('kuralays')

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def mercur_follow():
    insta_task('mercur_auto')

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def watchesforman__follow():
    insta_task('watches_for_man_')

#@periodic_task(run_every=crontab(hour='*/1', minute='10'))
#def tatoochoker__follow():
#    insta_task('tatoo_choker_kz')

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def saltiss__follow():
    insta_task('saltiss')

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def kkcreative__follow():
    insta_task('kkcreative.kz')

@periodic_task(run_every=crontab(hour='*/1', minute='10'))
def instarobot__follow():
    insta_task('movies.kz')

# @periodic_task(run_every=crontab(hour='*/1', minute='10'))
# def dauletkaliyev__follow():
#     client = Client.objects.get(username='dauletkaliyev')
#     print "STARTING LIKE ROUTINE FOR dauletkaliyev"
#     instagram_like(client)
#     print "FINISHED LIKE ROUTINE FOR dauletkaliyev"

@periodic_task(run_every=crontab(hour='*/4', minute='10'))
def feed_like():
    access_token = "46695212.7e028dd.e4ce491f1aff44cf8b4315af0293d9f8"
    api = InstagramAPI(access_token=access_token)
    media_list=api.user_media_feed(count=30)
    media_list=media_list[0]
    for media in media_list:
        time.sleep(1+random()+random())
        api.like_media(media.id)
        print "liked media %s" % media.id

# @periodic_task(run_every=crontab(minute=0, hour="*/12"))
# def followerstats_tetrus():
#     client = Client.objects.get(username='tetrus.club')
#     obj = FollowerStat(
#         client=client,
#         followers_num=client.get_followers_num()
#         )
#     obj.save()
#     client.follower_num = obj.followers_num
#     # client.follows_num = obj.follows_num
#     client.save()

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_duman():
    client = Client.objects.get(username='duman_rc')
    stat_record('duman_rc')

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_kuralays():
    client = Client.objects.get(username='kuralays')
    stat_record('kuralays')

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_mercur():
    client = Client.objects.get(username='mercur_auto')
    stat_record('mercur_auto')

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_watchesforman():
    client = Client.objects.get(username='watches_for_man_')
    stat_record('watches_for_man_')

# @periodic_task(run_every=crontab(minute=0, hour="*/12"))
# def followerstats_tatoochoker():
#     client = Client.objects.get(username='tatoo_choker_kz')
#     obj = FollowerStat(
#         client=client,
#         followers_num=client.get_followers_num()
#         )
#     obj.save()
#     client.follower_num = obj.followers_num
#     # client.follows_num = obj.follows_num
#     client.save()

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_saltiss():
    client = Client.objects.get(username='saltiss')
    stat_record('saltiss')

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_kkcreative():
    client = Client.objects.get(username='kkcreative.kz')
    stat_record('kkcreative.kz')

@periodic_task(run_every=crontab(minute=0, hour="*/12"))
def followerstats_instarobot():
    client = Client.objects.get(username='movies.kz')
    stat_record('movies.kz')

# @periodic_task(run_every=crontab(minute=0, hour="*/12"))
# def followerstats_dauletkaliev():
#     client = Client.objects.get(username='dauletkaliyev')
#     obj = FollowerStat(
#         client=client,
#         followers_num=client.get_followers_num()
#         )
#     obj.save()
#     client.follower_num = obj.followers_num
#     # client.follows_num = obj.follows_num
#     client.save()









