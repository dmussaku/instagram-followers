from django.db import models
from instagram.client import InstagramAPI
from random import randint, random
import time
import ast
from datetime import datetime
import pytz
from instagram_followers import settings
from django.db import transaction
ALMATY_TZ = pytz.timezone('Asia/Almaty')

class Client(models.Model):
    username = models.CharField(max_length=100)
    client_id = models.CharField(max_length=50)
    secret_key = models.CharField(max_length=50)
    access_token = models.CharField(max_length=70, null=True)
    user_id = models.CharField(max_length=20, null=True)
    # current_follower = models.OneToOneField('Follower', 
    #   related_name='current_follower', null=True, blank=True)
    current_user_id = models.IntegerField(null=False, default=0)
    last_date_paid = models.DateTimeField(null=True, blank=True)
    follower_num = models.IntegerField(null=False, default=0)
    follows_num = models.IntegerField(null=False, default=0)
    active = models.BooleanField(default=False)
    following = models.BooleanField(default=True)
    # followed_by = models.ForeignKey('Follower', related_name='follower',
    #   null=True, blank=True)

    def get_followers(self):
        if self.access_token:
            api = InstagramAPI(access_token = self.access_token)
            users=[]
            followers, next_ = api.user_followed_by()
            time.sleep(randint(2,3))
            [users.append(follower) for follower in followers]
            while (next_!=None):
                followers, next_ = api.user_followed_by(with_next_url=next_)
                time.sleep(randint(2,3))
                [users.append(follower) for follower in followers]
            return users
        else:
            return False

    def get_current_follower(self):
        if not self.current_follower:
            if not InstagramUser.objects.filter(client=self):
                return False
            else:
                self.current_follower = InstagramUser.objects.filter(client=self).first()
                self.save()
        return self.current_follower

    @property
    def added_in_one_day(self):
        stats = list(FollowerStat.objects.filter(client=self))
        size = len(stats)
        if size >= 3:
            return stats[size-1].followers_num - stats[size-3].followers_num
        elif size == 2:
            return stats[size-1].followers_num - stats[size-2].followers_num
        else:
            return 0

    def get_followers_num(self):
        api = InstagramAPI(access_token=self.access_token)
        users=[]
        followers, next_ = api.user_followed_by(self.user_id)
        time.sleep(1.2+random())
        [users.append(follower) for follower in followers]
        while next_:
                followers, next_ = api.user_followed_by(self.user_id, with_next_url=next_)
                time.sleep(1.2+random())
                [users.append(follower) for follower in followers]
        return len(users)

    def get_follows_num(self):
        api = InstagramAPI(access_token=self.access_token)
        users=[]
        followers, next_ = api.user_follows(self.user_id)
        time.sleep(1.2+random())
        [users.append(follower) for follower in followers]
        while next_:
                followers, next_ = api.user_follows(self.user_id, with_next_url=next_)
                time.sleep(1.2+random())
                [users.append(follower) for follower in followers]
        return len(users)

    def get_last_followers_num(self):
        if self.followerstat_set.all():
            return self.followerstat_set.last().followers_num
        else:
            return 0

    def __unicode__(self):
        return "%s %s" % (self.username, self.user_id)

    @classmethod
    def filter_by_followers(cls):
        queryset = list(cls.objects.filter(followerstat__isnull=False).distinct())
        obj_list = []
        max = queryset[0].followerstat_set.last()
        for i in range(0, len(queryset)-1):
            for obj in queryset:
                if obj.followerstat_set.last().followers_num>max.followers_num:
                    max = obj.followerstat_set.last()
            obj_list.append(max.client)
            queryset.remove(max.client)
            max = queryset[0].followerstat_set.last()
        obj_list.append(queryset[0])
        return obj_list


class InstagramUser(models.Model):
    client = models.ForeignKey(Client, null=True)
    user_id = models.CharField(max_length=20, null=False)
    username = models.CharField(max_length=100, null=False)
    full_name = models.CharField(max_length=100, null=True, blank=True)
    website = models.CharField(max_length=100, null=True, blank=True)
    date_added = models.DateTimeField(blank=True, auto_now_add=True)
    followers = models.CharField(max_length=10000, null=True)
    next_elem = models.CharField(max_length=30, null=True)
    finished = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s %s" % (self.username, self.user_id)

    @classmethod
    def save_from_instagram_user(cls, user_obj, client):
        instagram_user=cls(
            user_id=user_obj.id,
            username=user_obj.username,
            full_name=user_obj.full_name,
            client=client
            )
        instagram_user.save()

    def get_followers(self, client_id):
        client = Client.objects.get(id=client_id)
        api = InstagramAPI(access_token = client.access_token)
        users=[]
        followers, next_ = api.user_followed_by(self.user_id)
        time.sleep(randint(2,3))
        [users.append(follower) for follower in followers]
        for i in range(0,25):
                followers, next_ = api.user_followed_by(self.user_id, with_next_url=next_)
                time.sleep(randint(2,3))
                [users.append(follower) for follower in followers]
        return users

    def populate_users(self, client_id):
        users = []
        [users.append(follower.id) for follower in self.get_followers(client_id)]
        self.followers = str(users)
        self.save()

    def get_follower_list(self):
        if not self.followers:
            self.populate_users(self.client.id)
            self.save()
        return ast.literal_eval(self.followers)


class Follower(models.Model):
    user_id = models.CharField(max_length=20, null=False, unique=True)
    username = models.CharField(max_length=30, null=False, unique=True)

    def __unicode__(self):
        return str(self.id) + ' ' +str(self.user_id)

    def check_followed(self, client):
        if client in self.client_set.all():
            return True
        else:
            return False

    @classmethod
    def populate_followers(cls, client):
        i = 0
        api = InstagramAPI(access_token=client.access_token)
        followers, next_ = api.user_followed_by(settings.INSTAVIDEO_KAZ_ID)
        i+=len(followers)
        time.sleep(1+random()+random())
        with transaction.atomic():
            for follower in followers:
                f = Follower(
                    user_id=follower.id,
                    username=follower.username
                    )
                try:
                    f.save()
                except:
                    pass
        print 'at %s followers' % i
        while next_:
            followers, next_ = api.user_followed_by(settings.INSTAVIDEO_KAZ_ID, with_next_url=next_)
            time.sleep(1+random()+random())
            with transaction.atomic():
                for follower in followers:
                    f = Follower(
                        user_id=follower.id,
                        username=follower.username
                        )
                    try:
                        f.save()
                    except:
                        pass
            print "Followers num is %s" % (len(Follower.objects.all()))
            i+=len(followers)
            print 'at %s followers' % i

import json
class FollowerStat(models.Model):
    client = models.ForeignKey(Client)
    followers_num = models.IntegerField(default=0)
    follows_num = models.IntegerField(default=0)
    date = models.DateTimeField(blank=True)

    def save(self, *args, **kwargs):
        self.date = ALMATY_TZ.localize(datetime.now()).today()
        super(self.__class__, self).save(*args, **kwargs)

    def get_tuple(self):
        return (int(self.followers_num), self.date.date().__str__())

    def get_dict(self):
        return dict(
                date=self.date.ctime(),
                followers_num=self.followers_num
                )

    def __unicode__(self):
        return "%s %s %s" % (self.client, self.followers_num, self.date)
