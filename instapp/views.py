from django.shortcuts import render
from django.http import HttpResponse
from instapp.models import Client, FollowerStat
import json
from random import randint

def log_view(request):
	if request.method == 'GET':
		f = open('../logs/celeryd.log', 'r')
		text = f.readlines()
		response=[]
		for t in text:
			response.append(t+'<br/>')
		return HttpResponse(response)

# Create your views here.

def follower_statistics(request):
	if request.method == 'GET' or request.is_ajax():
		if request.GET:
			username = request.GET.get('username','')
			if not username:
				return HttpResponse('none')
			try:
				client = Client.objects.get(username=username)
				follower_stats = FollowerStat.objects.filter(client=client)
				if not follower_stats:
					return HttpResponse('none')
			except:
				return HttpResponse('none')
			obj_list = [f.get_dict() for f in follower_stats]
			if len(follower_stats) == 1:
				return HttpResponse('none')
			first = follower_stats.first()
			last = follower_stats.last()
			average = float(last.followers_num - first.followers_num) / (last.date-first.date).days
			average = average*100/400
			obj_dict = {'obj_list':obj_list, 'average':average}
			return HttpResponse(
				json.dumps(obj_dict), content_type="application/json")
		else:
			return HttpResponse('none')
	else:
		return HttpResponse('none')

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def stats(request):
	NUM_OF_VALUES = 20
	if request.method == 'GET':
		return render(
			request, 'statistics.html', 
			{'clients':Client.filter_by_followers(),
			 'stats':[]
			}
			)
	if request.method == 'POST':
		username = request.POST.get('username')
		if not username:
			return render(
				request, 'statistics.html', 
				{'clients':Client.filter_by_followers()}
				)
		try:
			client = Client.objects.get(username=username)
		except:
			return render(
				request, 'statistics.html', 
				{'clients':Client.filter_by_followers()}
				)
		from_date = request.POST.get('from_date',"")
		to_date = request.POST.get('to_date',"")
		if from_date and to_date:
			stats = FollowerStat.objects.filter(
				client=client).filter(
					date__range=[from_date, to_date])
		else:
			stats = FollowerStat.objects.filter(
				client=client)
		print stats
		if len(stats)>NUM_OF_VALUES:
			new_stats = list(chunks(stats, len(stats)/NUM_OF_VALUES))
			for i in range(0, len(new_stats)-NUM_OF_VALUES):
				new_stats.pop(randint(0, len(new_stats)-1))
			new_stats = [obj[len(obj)/2] for obj in new_stats]
			if stats[0] not in new_stats:
				new_stats[0] = stats[0]
			if stats[len(stats)-1] not in new_stats:
				new_stats[len(new_stats)-1] = stats[len(stats)-1]
			stats = new_stats
		if not stats:
			return render(
				request, 'statistics.html', 
				{'clients':Client.filter_by_followers()}
				)
		followers = [int(obj.followers_num) for obj in stats]
		last = len(followers)-1
		added = followers[last] - followers[0]
		num_days = (stats[last].date-stats[0].date).days
		if num_days == 0:
			num_days = 1
		average = float(followers[last] - followers[0]) / num_days
		average = average*100/400
		dates = [obj.date.ctime() for obj in stats]
		days = (stats[last].date-stats[0].date).days
		return render(
			request, 'statistics.html',
			{'clients':Client.filter_by_followers(),
			 'followers':followers,
			 'dates':dates,
			 'average':average,
			 'added':added,
			 'days':days,
			 'client':client,
			 'username':username,
			 'from_date':from_date,
			 'to_date':to_date
			}
			)

def index(request):
	return render(request, 'index.html')
