# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0011_remove_client_current_follower'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='follower_num',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='follows_num',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='followerstat',
            name='follows_num',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
