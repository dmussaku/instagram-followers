# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0006_follower_username'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='followed_by',
        ),
        migrations.AlterField(
            model_name='client',
            name='current_follower',
            field=models.OneToOneField(related_name='current_follower', null=True, blank=True, to='instapp.Follower'),
            preserve_default=True,
        ),
    ]
