# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0008_auto_20150312_0750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='follower',
            name='user_id',
            field=models.CharField(unique=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='follower',
            name='username',
            field=models.CharField(unique=True, max_length=30),
            preserve_default=True,
        ),
    ]
