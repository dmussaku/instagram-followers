# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0010_auto_20150316_1148'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='current_follower',
        ),
    ]
