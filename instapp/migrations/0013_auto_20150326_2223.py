# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0012_auto_20150326_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followerstat',
            name='followers_num',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='followerstat',
            name='follows_num',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
