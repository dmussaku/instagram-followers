# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0002_auto_20141208_1007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followerstat',
            name='date',
            field=models.DateTimeField(blank=True),
            preserve_default=True,
        ),
    ]
