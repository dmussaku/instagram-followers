# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0005_auto_20150311_1907'),
    ]

    operations = [
        migrations.AddField(
            model_name='follower',
            name='username',
            field=models.CharField(default='asd', unique=True, max_length=30),
            preserve_default=False,
        ),
    ]
