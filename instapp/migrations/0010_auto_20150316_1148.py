# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0009_auto_20150312_1358'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='current_user_id',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='last_date_paid',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
