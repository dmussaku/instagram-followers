# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='instagramuser',
            name='next_element',
        ),
        migrations.AddField(
            model_name='instagramuser',
            name='next_elem',
            field=models.CharField(max_length=30, null=True),
            preserve_default=True,
        ),
    ]
