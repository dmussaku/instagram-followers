# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100)),
                ('client_id', models.CharField(max_length=50)),
                ('secret_key', models.CharField(max_length=50)),
                ('access_token', models.CharField(max_length=70, null=True)),
                ('user_id', models.CharField(max_length=20, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FollowerStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('followers_num', models.IntegerField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('client', models.ForeignKey(to='instapp.Client')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InstagramUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.CharField(max_length=20)),
                ('username', models.CharField(max_length=100)),
                ('full_name', models.CharField(max_length=100, null=True, blank=True)),
                ('website', models.CharField(max_length=100, null=True, blank=True)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('followers', models.CharField(max_length=10000, null=True)),
                ('next_element', models.CharField(max_length=30, null=True)),
                ('finished', models.BooleanField(default=False)),
                ('client', models.ForeignKey(to='instapp.Client', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='client',
            name='current_follower',
            field=models.OneToOneField(related_name='current_follower', null=True, blank=True, to='instapp.InstagramUser'),
            preserve_default=True,
        ),
    ]
