## Instagram followers ##
### You will need virtualenv for version control ###
```
#!shell


>>> vritualenv instagram_followers
>>> cd instagram_followers
>>> . ./bin/activate
>>> git clone git@bitbucket.org:dmussaku/instagram-followers.git
>>> cd instagram_followers
>>> git fetch origin
>>> git pull
>>> sudo apt-get update
>>> sudo apt-get install rabbitmq-server
>>> sudo apt-get install mysql-server
>>> sudo apt-get install nginx
>>> sudo apt-get install libxml2-dev libxslt-dev  python-dev  python-setuptools
>>> sudo apt-get install libmysqlclient-dev
>>> pip install -r requirements.txt
```

To get an access token, download python instagram from here [https://github.com/Instagram/python-instagram](https://github.com/Instagram/python-instagram) and run get_access_token.py, input prompted values, then follow instructions. For the scopes input 'comments relationships likes'. Save access token somewhere, haven't decided where yet, either settings.py or i will store it in database.

Manage your instagram clients on this link [http://instagram.com/developer/clients/manage/](http://instagram.com/developer/clients/manage/) (you can find information on that page about instagram api (limitations, authentications etc.)